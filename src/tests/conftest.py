from pathlib import Path

import pytest
import yaml

from src.app import init_app


@pytest.fixture
def config():
    config = yaml.safe_load(Path('config/tests.yml').read_text())
    yield config


@pytest.fixture
def client(loop, aiohttp_client, config):
    app = init_app(config)
    return loop.run_until_complete(aiohttp_client(app))


