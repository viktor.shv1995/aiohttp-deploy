async def test_handler(client):
    response = await client.post(
        "/check/status",
        json={"timeout": "0", "url": "http://google.com"}
    )
    assert response.status == 200
