from dataclasses import dataclass, field
from http import HTTPStatus
from typing import Optional

import marshmallow
from marshmallow_dataclass import class_schema


@dataclass
class SiteStatusRequest:
    """Объект запроса статуса сайта.

    Эквивалентная схема marshmallow:
    >>>class SiteStatusRequestSchema(marshmallow.Schema):
    >>>    url: marshmallow.fields.Url(required=True)
    >>>    timeout = marshmallow.fields.Float(validate=marshmallow.validate.Range(min=0))
    """

    url: str = field(metadata={'marshmallow_field': marshmallow.fields.Url(required=True)})
    timeout: Optional[float] = field(metadata={'validate': marshmallow.validate.Range(min=0)})


_max_http_status = 600


@dataclass
class SiteStatusResponse:
    """Результат проверки статуса сайта.

    Эквивалентная схема marshmallow:
    >>>class SiteStatusResponseSchema(marshmallow.Schema):
    >>>    http_status: marshmallow.fields.Integer(
    >>>     validate=marshmallow.validate.Range(min=200, max=600),
    >>>    )
    >>>    error: marshmallow.fields.Boolean(required=True)
    """

    error: bool
    http_status: Optional[int] = field(
        metadata={'validate': marshmallow.validate.Range(
            min=HTTPStatus.OK.value, max=_max_http_status,
        )},
    )


SiteStatusRequestSchema = class_schema(SiteStatusRequest)
SiteStatusResponseSchema = class_schema(SiteStatusResponse)
