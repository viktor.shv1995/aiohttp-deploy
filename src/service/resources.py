from aiohttp import web
from aiohttp.client import ClientSession, ClientTimeout, TCPConnector

from src.service.tracing import build_log_trace_config


async def setup_client_session(app: web.Application):
    """Настраивает http клиента."""
    cli = ClientSession(
        connector=TCPConnector(force_close=True),
        timeout=ClientTimeout(total=app['config']['client']['timeout']),
        trace_configs=[
            build_log_trace_config(),
        ],
    )
    app['http_cli'] = cli
    yield
    await cli.close()
