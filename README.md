# aiohttp.practice

Данный репозиторий предназначен для демонстрации промышленных практик разработки бэкенд сервисов с использованием библиотеки aiohttp.

Реализовано:
- middleware
- трейсинг клиентских запросов
- валидация входящих запросов
- документация АПИ

Функционал:
- проверка статуса сайта

# Разработка

## Рабочее окружение
Для начала разработки необходимо настроить рабочее окружение. Нам понадобятся следующие системные зависимости: 
- [python](https://www.python.org/downloads/) версии 3.7 или выше
- менеджер зависимостей [poetry](https://python-poetry.org/docs/#installation) версии 1.0 или выше

Настройка окружения:
1. Настроить репозиторий
    ```shell script
    git clone https://gitlab.com/f1376/aiohttp.practice.git aiohttp.practice
    cd aiohttp.practice
    ```
2. Установить зависимости. Зависимости установятся в виртуальное окружение.
    ```shell script
    poetry install
    ```

## Запуск

Подключение виртуального окружения
```shell script
poetry shell
```

Из виртуального окружения сервис запускается командой
```shell script
python -m src.app -c src/config.yml
```

## Внесение изменений

Все изменения должны проходить проверку линтером
```shell script
flake8 src
```

При изменении АПИ нужно обновить swagger документацию:
- откорректировать версию сервиса и запустить скрипт [tools/apispec/generate.py](tools/apispec/generate.py).
    Результатом будет файл api_config.yaml в соответствующей папке в [public/docs](./public/docs).
- добавить ссылку в [index.html](./public/index.html) на `api_view.html` новой версии
