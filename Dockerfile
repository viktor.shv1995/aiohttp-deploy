FROM library/python:3.7-slim

RUN DEBIAN_FRONTEND=noninteractive apt-get update && apt-get install --no-install-recommends -y build-essential

RUN mkdir -p /aiohttp_service
WORKDIR /aiohttp_service

COPY . /aiohttp_service

RUN python -m pip install --upgrade pip
RUN pip install poetry==1.1.14
RUN poetry config virtualenvs.create false && poetry install

EXPOSE 8080

CMD python -m src.app -c src/config/config.yml